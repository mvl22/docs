title: OAuth2 Client Credentials

# Registering OAuth2 Client Credentials

The Raven OAuth2 service allows websites to authenticate using the
[OAuth2](https://oauth.net/2/) protocol. In order to make use of the OAuth2
protocol you will need to register some *client credentials*.

OAuth2 requires that an application authenticating a user identify itself to the
authentication service. Client credentials are the way in which your website
identifies itself to Raven when requesting user sign in.

Client credentials consist of two parts. The **client id**, sometimes called the
**client key**, is the equivalent of a username identifying your application.
The **client secret** is the equivalent of a password. As the name suggests, it
should be kept secret.

## Register a Google account

Raven makes use of OAuth2 client credentials managed by Google. There are,
somewhat confusingly, several forms of identity in the Google ecosystem:

An **ordinary Google account** is the sort of Google account you can register
for free at https://google.com/. All that you need to register an ordinary
Google account is the ability to receive email. An ordinary Google account is
associated with an email address.

A **G Suite account** is like an ordinary Google account but the associated email
address ends in `@cam.ac.uk`. G Suite accounts use Raven to authenticate
rather than giving a password to Google. A G Suite account is associated with a
single individual.

A **service account** is an account associated with a particular “project” in
Google’s [developer console](https://console.developers.google.com/). They are
_not_ associated with individuals. They do have email addresses associated with
them which end with `.gserviceaccount.com`. There is no mailbox behind the
email address associated with service accounts.

In order to register new OAuth2 client credentials you will need to create an
*ordinary Google account*. If you're just experimenting you can use any personal
GMail account. If you're deploying a site in production we **strongly**
recommend that you create a shared Google account for a role address. For
example, the COs of St Botolph's college would create a Google account for
`computer-officers@botolphs.cam.ac.uk` and store the credentials like they do
with any other shared secrets. By using a role address, you can have some
confidence that access to the Google account can be recovered in future as
people join or leave your team.

## Create a Google project

Once registered, sign in as the shared user, open the [Google developer
console](https://console.developers.google.com/) and create a new project for
your service; click **Select a project**, then **NEW PROJECT**, enter a name
for the project and, optionally, edit the provided project ID. Click **Create**.

!!! note "G Suite accounts cannot create projects in the Google developer console"
    This is intentional. It is easy to miss which Google user you are currently
    signed in as and the G Suite administrators want to make it very hard for
    projects to be created by an individual who forgets to grant owner access to
    the rest of their team. Such projects are in danger of becoming "orphaned"
    when the individual who created it leaves the University.

You will first need to add an initial set of administrators:

1. Navigate to the [IAM
   page](https://console.developers.google.com/iam-admin/iam) of the Google
   developer console for your project.
2. Use the **Add** button at the top of the page to add some G Suite users to
   the project and give them the **Project Owner** role.
   (Recall that users with email addresses of the form `...@cam.ac.uk` are part
   of the University's G Suite account and are authenticated via Raven sign in.)
3. Sign out from the shared role account and proceed using one of the G Suite
   accounts you just added to the project as an administrator.

!!! important "All project administration should now be done as a G Suite user"
    It is best practice to use the shared Google account sparingly. As people
    join or leave your team, their corresponding G Suite user can be added or
    removed to the set of project owners.

    Sign in details for the shared account should be kept securely for disaster
    recovery purposes or for adding project owners if all existing project
    owners are unavailable.

## Create the client credentials

Once you have a new Google project, you can create some OAuth2 client
credentials for your first application.

1. Open the [Google API Console Credentials
   page](https://console.developers.google.com/apis/credentials).
2. On the Credentials page, select **Create credentials**, then **OAuth client
   ID**.
3. You may be prompted to set a product name on the Consent screen; if so, click
   **Configure consent screen** and supply the information requested on that
   page. Unless your project has been created as part of the University of
   Cambridge organisation, choose "External" user authentication. You will also
   need to add the domain name of any websites providing Raven OAuth2 sign in.
   Leave the set of [scopes](apache-oauth2.md#oauth2-scopes) as the default set.
   Click "Save" to return to the Credentials screen.
4. Select **Web Application** for the **Application Type**. If the web
   application or web server you are configuring specifies JavaScript origins or
   redirect URIs enter them here.
5. Click **Create**.
6. On the page that appears, make a note of the **client ID** and **client
   secret**. You will need them to configure your site.
